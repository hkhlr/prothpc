# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT


#!/bin/bash

my_super_variable=-1

if [ $my_super_variable -gt 0 ]; then
  echo "my_super_variable is positive"
elif [ $my_super_variable -eq 0 ]; then
  echo "my_super_variable is 0"
else
  echo "my_super_variable is negative"
fi
