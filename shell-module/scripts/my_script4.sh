# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT


#!/bin/bash

for var in {1..5} some_word; do
  echo "Print var = $var"
done
