# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT


#!/bin/bash

# This line is a comment. It will *not* be interpreted by the shell.

echo 'Hello, world! I am a shell script.'
