#!/bin/bash

# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT

#SBATCH --job-name=regular_openmp
#SBATCH --nodes=1
#SBATCH --ntasks=1                # one process that wil fork the threads
#SBATCH --cpus-per-task=12        # one thread per cpu core
#SBATCH --error=%u.err.%J.log
#SBATCH --output=%u.out.%J.log
# #SBATCH --partition=regular    # specific to the HPC cluster in Giessen
#SBATCH --time=0-00:05:00


# *** Adapt this to your needs ***
module purge              # clear current environment
### load modules needed by your app ###
module add ALL-SUBMODULES # make all top-level modules available
module add slurm          # you will always need this one

echo "Starting at `date`"
echo "Running on hosts: $SLURM_NODELIST"
echo "Current working directory is `pwd`"

# Set the number of threads to use for OpenMP. Since we want to assign *one* thread to each core we
# set this to the value specified above with --cpus-per-task. 
# NOTE: Depending on the application at use the required environment variable might be different.
# E.g.:
#      *  OpenBLAS linear algebra library: OPENBLAS_NUM_THREADS
#      *  Intel's MKL                    : MKL_NUM_THREADS
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

srun path/to/your/OpenMP/parallel/application

echo "Finished at `date`"
