#!/bin/bash

# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT

#SBATCH --job-name=MPI_parallel
#SBATCH --time=0-0:10:00
# #SBATCH --partition=regular
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=24
#SBATCH --error=%u.err.%J.log
#SBATCH --output=%u.out.%J.log

# load all modules needed for the current run
module purge                         # clean the current env

module add ALL-SUBMODULES # make all top-level modules available
module add slurm                                    # we always need this one
module add openmpi/3.1.5/gcc/9.2.0                  # needed for multi-processing

echo "Starting at `date`"
echo "Running on hosts: $SLURM_NODELIST"
echo "Current working directory is `pwd`"

srun path/to/your/OpenMPI/parallel/application

echo "Finished at `date`"
