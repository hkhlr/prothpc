# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT


#!/bin/bash

sum=0

for outfile in task2_*.out; do
    add=`cat $outfile`
    sum=$(($sum + $add))
done
echo $sum
