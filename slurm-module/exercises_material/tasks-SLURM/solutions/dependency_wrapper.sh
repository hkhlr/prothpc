# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT


#!/bin/bash

jid1=$(sbatch array1.slurm | head -n 1 | awk '{FS=" ";print$4}')
echo "Submitted job ID $jid1"

jid2=$(sbatch --dependency=aftercorr:$jid1 array2.slurm | head -n 1 | awk '{FS=" ";print$4}')
echo "Submitted job ID $jid2"

jid3=$(sbatch --dependency=afterok:$jid2 sum.slurm | head -n 1 | awk '{FS=" ";print$4}')
echo "Submitted job ID $jid3"
