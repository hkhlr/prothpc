# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT


#!/bin/bash

echo $(($1 + $1))
