# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT


#!/bin/bash

jid1=$(sbatch dependency_1.slurm | head -n 1 | awk '{FS=" ";print$4}')
echo "Submitted job ID $jid1"
jid2=$(sbatch --dependency=afterany:$jid1 dependency_2.slurm | head -n 1 | awk '{FS=" ";print$4}')
echo "Submitted job ID $jid2"
