import tensorflow as tf
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, Flatten

import pandas as pd

from input_data import *

from helper import parse_command_line_args

import matplotlib.pyplot as plt

os.environ["KERAS_BACKEND"] = "tensorflow"


def define_cnn_model(input_shape, num_units, num_filters,
                     kernel_size=(2, 2), pool_size=(2, 2), conv_layers=1, dense_layers=1):
    model = tf.keras.models.Sequential()
    # Feature extraction region of model
    model.add(Conv2D(filters=num_filters, kernel_size=kernel_size,
                     activation='relu', input_shape=input_shape))
    model.add(MaxPooling2D(pool_size=pool_size))

    for i in range(1, conv_layers):
        model.add(Conv2D(filters=num_filters, kernel_size=kernel_size,
                         activation='relu'))
        model.add(MaxPooling2D(pool_size=pool_size))

    # Classification region of model
    model.add(Flatten())
    for i in range(dense_layers):
        model.add(Dense(units=num_units, activation='relu'))
    model.add(Dense(units=11, activation='softmax'))
    print(model.summary())
    return model


def main():
    # Parse command line argments
    ARGS = parse_command_line_args()

    # Get the data
    train_data, val_data, test_data = \
        get_input_data(ARGS.data_dir, ARGS.not_use_all_data, ARGS.validation_split_size)

    # Define model
    model = define_cnn_model(input_shape=IMAGE_SIZE,
                             num_units=ARGS.num_units,
                             num_filters=ARGS.num_filters,
                             conv_layers=ARGS.conv_layers,
                             dense_layers=ARGS.dense_layers)

    # Set up the model
    opt = tf.keras.optimizers.SGD(learning_rate=ARGS.learning_rate)
    model.compile(optimizer=opt,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    keras_callbacks = []

    if ARGS.early_stopping:
        keras_callbacks.append(tf.keras.callbacks.EarlyStopping(monitor='loss', patience=ARGS.early_stopping_patience))

    if ARGS.augment_data:
        train_data = train_data.map(
            augment_tf,
            num_parallel_calls=tf.data.experimental.AUTOTUNE, deterministic=False)

        # tf.py_function can execute arbitrary python code
        # train_data = train_data.map(
        #    lambda x, y: (tf.py_function(augment_py, [x], [tf.float32]), y),
        #    num_parallel_calls=tf.data.experimental.AUTOTUNE, deterministic=False)

        # num_parallel_calls=tf.data.experimental.AUTOTUNE
        # autotune = num of CPUs available

        # need to set shape as tf can not infer it (an augmentation may result in another img size)
        train_data = train_data.map(set_shape)

    if ARGS.augment_test:
        test_data = test_data.repeat(3).map(augment_tf, num_parallel_calls=tf.data.experimental.AUTOTUNE,
                                            deterministic=False).map(set_shape)

    if ARGS.show_image:
        subset = train_data.take(3)
        print(subset)
        for image, label in subset:
            print(image.shape)
            ### #show first img from batch
            plt.imshow(image)
            plt.show()
        exit()

    train_data = train_data.shuffle(buffer_size=10000, reshuffle_each_iteration=True).batch(ARGS.batch_size)
    train_data = train_data.prefetch(20)
    val_data = val_data.batch(ARGS.batch_size)
    # Train the model using the train dataset
    train_history = model.fit(
        train_data,
        shuffle=True,
        validation_data=val_data,
        epochs=ARGS.num_epochs,
        verbose=1,
        callbacks=keras_callbacks
    )

    # Evaluate model performance on the test dataset
    test_data = test_data.batch(ARGS.batch_size)
    test_loss, test_accuracy = model.evaluate(test_data, verbose=True)

    os.makedirs(ARGS.result_dir, exist_ok=True)
    with open(ARGS.result_dir + '/' + 'test_accuracy.txt', 'w') as f:
        print("Test accuracy : {:.2f}%".format(test_accuracy * 100.0), file=f)

    pd.DataFrame(data=train_history.history,
                 index=range(1, len(train_history.history['loss']) + 1)).to_csv(
        ARGS.result_dir + '/' + 'train_hist.csv',
        index_label='epoch'
    )


if __name__ == "__main__":
    main()
