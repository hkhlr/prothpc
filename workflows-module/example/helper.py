import argparse
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import os


# helper function used in the excercises

def parse_command_line_args():
    parser = argparse.ArgumentParser(
        description='train a basic Convolutional Neural Network')
    parser.add_argument("--num_epochs", type=int, default=1,
                        required=False, action='store',
                        help="Number of epochs to train the NN.")
    parser.add_argument("--batch_size", type=int, default='32',
                        required=False, action='store',
                        help="Number of examples in batch.")
    parser.add_argument("--learning_rate", type=float, default=1e-2,
                        required=False, action='store',
                        help="Learning rate used for gradient descent.")
    parser.add_argument("--validation_split_size", action='store',
                        type=float, required=False, default=0.2)
    parser.add_argument("--conv_layers", type=int, default=1,
                        required=False, action='store',
                        help="Number of conv layers.")
    parser.add_argument("--dense_layers", type=int, default=1,
                        required=False, action='store',
                        help="Number of dense Layers.")
    parser.add_argument("--early_stopping", action='store_true',
                        default=False, required=False,
                        help="use early stopping")
    parser.add_argument("--early_stopping_patience",type=int, action='store',
                        default=3, required=False,
                        help="patience to use for early stopping")
    parser.add_argument("--show_image", action='store_true',
                        default=False, required=False,
                        help="show an image of the training dataset and exit")
    parser.add_argument("--not_use_all_data", action='store_true',
                        default=False, required=False,
                        help="Not Use all available training data")
    parser.add_argument("--result_dir", type=str, default='./result',
                        required=False, action='store',
                        help="where to store the results (result= recorded history of loss and accuracy)")
    parser.add_argument("--data_dir", type=str, default='./DATA',
                        required=False, action='store',
                        help="Path to the data location")
    parser.add_argument("--augment_data", required=False, default=False,
                        action='store_true',
                        help="Activate data augmentation by rotating and shifting images")
    parser.add_argument("--augment_test", required=False, default=False,
                        action='store_true',
                        help="Activate test data augmentation by rotating and shifting images")
    parser.add_argument("--num_filters", type=int, default=16,
                        required=False, action='store')
    parser.add_argument("--num_units", type=int, default=64,
                        required=False, action='store')
    args = parser.parse_args()
    return args


def plot_lossacc_vs_epochs(histdict, export_to_pdf=False):
    train_acc = histdict['accuracy']
    train_loss = histdict['loss']
    try:
        val_loss = histdict['val_loss']
        val_acc = histdict['val_accuracy']
    except KeyError:
        pass

    epochs = range(1, len(train_acc) + 1)

    fig = plt.figure(figsize=(10, 5))

    # Loss
    ax1 = fig.add_subplot(1, 2, 1)
    ax1.set_title('Training and validation loss')
    ax1.set_xlabel('Epoch')
    ax1.set_ylabel('Loss')
    ax1.minorticks_on()
    ax1.plot(epochs, train_loss, 'bo-', label='Train loss')
    ax1.plot(epochs, val_loss, 'rd--', label='Val loss')
    ax1.legend()

    # Accurracy
    ax2 = fig.add_subplot(1, 2, 2)
    ax2.set_title('Training and validation accuracy')
    ax2.set_xlabel('Epoch')
    ax2.set_ylabel('Accuracy')
    ax2.minorticks_on()
    ax2.plot(epochs, train_acc, 'bo-', label='Train acc')
    ax2.plot(epochs, val_acc, 'rd--', label='Val acc')
    ax2.legend()

    if export_to_pdf:
        plt.savefig('results_lossacc_vs_epochs.pdf',
                    bbox_inches='tight')
    else:
        plt.show()
