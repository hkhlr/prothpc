#!/bin/bash

#SBATCH --ntasks 1
#SBATCH --cpus-per-task 8
#SBATCH --mem-per-cpu=3800
#SBATCH --time 00:30:00
# these parameters determining the size of our job
# meaning the amount of ressources needed to process it

#SBATCH --array 1-6
# This is a job Array of 6 jobs

#SBATCH --job-name Tensorflow
# A name for this job to be displayed in the queue
#SBATCH --output job_%A_%a.out
# filename where the terminal output of this job goes

# change to where the files are located
EXPERIMENT_DIR=./

# deactivate any current environment
if [ ! -z "$VIRTUAL_ENV" ]; then
deactivate
fi

#load the required modules
module purge
module load gcc/8.5 python/3.10

# activate the correct environment
source $EXPERIMENT_DIR/venv/bin/activate

echo $(cat $EXPERIMENT_DIR/parameter/$SLURM_ARRAY_TASK_ID.params)
python3 $EXPERIMENT_DIR/cnn_tf.py $(cat $EXPERIMENT_DIR/parameter/$SLURM_ARRAY_TASK_ID.params) --data_dir=$EXPERIMENT_DIR/DATA --result_dir=$EXPERIMENT_DIR/results/result_$SLURM_ARRAY_TASK_ID
# so that we know which parameters where used when viewing the output
cp $EXPERIMENT_DIR/parameter/$SLURM_ARRAY_TASK_ID.params $EXPERIMENT_DIR/results/result_$SLURM_ARRAY_TASK_ID/input_parameter.params

deactivate
