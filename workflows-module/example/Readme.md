
# Steps of the example deep learning workflow

* [File Transfer] create a directory where the scripts are located on the cluster
* [File Transfer] transfer the scripts to the cluster (`rsync`) or clone the repository with the scripts on the cluster (`git clone`)
* [File Transfer] Download the data : 
```
wget https://hessenbox.tu-darmstadt.de/dl/fi7C8UBvBLLMwLFcsk7UkRWC/hkhlr_dl_workflow_sample_data.tar.gz
```
* [General Preproccessing] untar the data into a directory ```tar -xzf hkhlr_dl_workflow_sample_data.tar.gz```
* [General Preproccessing] setup environment (at least ``python 3.9``): 
```
python3 -m venv ./venv
source venv/bin/activate
pip install --upgrade pip
pip install --upgrade keras-cv tensorflow pandas matplotlib scipy
```
* [General Preproccessing] start a SMALL test run to check if setup is correct
  * use ``--not_use_all_data`` as a command line argument to have only a small run
  * Other important command line args to ``cnn_tf.py`` script:
    * ``--data_dir``: Path to where the data was extracted
    * ``--result_dir``: directroy, wehre the result files will be written
* [Run Specific Preprocessing] Prepare parameters for multiple runs: 
  * run ``param_generator.py`` script, the location of the parameter files can be specified with the ``--result_dir`` argument
* [Run Specific Preprocessing] Review ``job_script.sh`` adjust settings if necessary
  * one may need to adjust the paths to the environment/data/result directories 
* [Run Specific Preprocessing] run a SMALL test job to check setup
  * remember to use ``--not_use_all_data``
  * do not yet launch the ful SLURM job array, this step is only for testing if the script was set up correctly
  * the ``job_script.sh`` hase some errors. (hint: check the python version)
  * be mindful of efficiency: does the ``job_script.sh`` use the cpu ressources efficiently?
* [Run calculations] run all jobs by launching the full SLURM job array
* [General Preprocessing/Aggregation] run ``visualization.py``. This script tires to read al subdirectories of ``--input_dir`` and collect the results into one big plot.
* [File transfer] download the resulting image



Note:
The original data can be found here:
http://ufldl.stanford.edu/housenumbers/
