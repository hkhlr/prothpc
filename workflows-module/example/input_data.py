import tensorflow as tf
import keras_cv
import scipy.io
import numpy as np
import os

# Handles the loading of the input data

# Constants
IMAGE_SIZE = (32, 32, 3)


# to specify the shape of our data, as it cannot be computed by tf automatically if we use augmentation
@tf.function
def set_shape(x, y):
    x = tf.reshape(x, (32, 32, 3))
    y.set_shape([11])
    return x, y


@tf.function
def normalize(image, label):
    return image / 255.0, label


def get_input_data(DATA_PATH, not_use_extra=True, val_percent=0.1):
    # load svhn data from the specified folder
    fname = DATA_PATH + '/train_32x32.mat'
    assert os.path.isfile(fname) and "Did not find Input data at the specified location"

    input = scipy.io.loadmat(fname)

    x_train = input["X"][:]
    y_train = input["y"][:].flatten()
    # first dim is batch dimension
    # need to reshape as input has last dim as batch size
    x_train = np.moveaxis(x_train, -1, 0)

    if not not_use_extra:
        fname = DATA_PATH + '/extra_32x32.mat'
        assert os.path.isfile(fname) and "Did not find Input data at the specified location"
        test = scipy.io.loadmat(fname)
        x_extra = test["X"][:, :, :, :]
        y_extra = test["y"][:].flatten()
        x_extra = np.moveaxis(x_extra, -1, 0)
        # just include the extra data at the end (tf will shuffle anyway)
        x_train = np.concatenate((x_train, x_extra))
        y_train = np.concatenate((y_train, y_extra))

    # use some part of the data as validation data
    val_samples = int(len(y_train) * val_percent)
    x_val = x_train[:val_samples]
    y_val = y_train[:val_samples]
    x_train = x_train[val_samples:]
    y_train = y_train[val_samples:]

    fname = DATA_PATH + '/test_32x32.mat'
    assert os.path.isfile(fname) and "Did not find Input data at the specified location"
    test = scipy.io.loadmat(fname)
    x_test = test["X"][:, :, :, :]
    y_test = test["y"][:].flatten()

    y_train = tf.keras.utils.to_categorical(y_train, num_classes=11)
    y_val = tf.keras.utils.to_categorical(y_val, num_classes=11)
    y_test = tf.keras.utils.to_categorical(y_test, num_classes=11)

    x_test = np.moveaxis(x_test, -1, 0)
    # also works as a sanity check to test if len(y)=len(x)

    x_train = x_train.astype(np.float32)
    x_val = x_val.astype(np.float32)
    x_test = x_test.astype(np.float32)

    train_data = tf.data.Dataset.from_tensor_slices((x_train, y_train)).map(normalize)
    val_data = tf.data.Dataset.from_tensor_slices((x_val, y_val)).map(normalize)
    test_data = tf.data.Dataset.from_tensor_slices((x_test, y_test)).map(normalize)

    return train_data, val_data, test_data


max_angle = -45
min_angle = 45
min_translate = -5
max_translate = +5

augmenter = keras_cv.layers.Augmenter([keras_cv.layers.RandAugment(value_range=(0, 1),magnitude=0.25)])


@tf.function
def augment_tf(image, label):
    return augmenter(image), label
