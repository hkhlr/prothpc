! SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
! SPDX-License-Identifier: MIT

module array
    use iso_fortran_env
    implicit none

    type matrix_real64 
        integer :: nrows, ncols
        real(real64), allocatable :: data(:,:)
    end type    

    contains

    subroutine create_matrix64(m, nrows, ncols, init_value)
        use iso_fortran_env
        implicit none

        integer,             intent(in)    :: nrows, ncols
        real(kind=real64),   intent(in)    :: init_value
        type(matrix_real64), intent(inout) :: m
        
        m%nrows = nrows
        m%ncols = ncols
        allocate(m%data(m%nrows, m%ncols))
        m%data = init_value
    end subroutine

end module

program test
    use iso_fortran_env
    use array
    implicit none
    
    integer, parameter  :: nrows = 3, ncols = 3
    type(matrix_real64) :: m

    call create_matrix64(m, nrows, ncols, 0._real64)

    if (allocated(m%data)) deallocate(m%data)

end program
