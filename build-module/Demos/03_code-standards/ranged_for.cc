/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

#include <iostream>
#include <vector>

template <typename T>
void print_vector(const std::vector<T>& v) {
    std::cout << "[ ";
    for (const auto& x : v)
        std::cout << x << " ";
    std::cout << "]" << std::endl;
}

// Trailing return types for functions: C++14 upwards
auto main() -> int {
    // int main() {
    std::vector<int> v = {1, 2, 3, 4, 5};

    // Print vector elements
    print_vector(v);
}
