/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

#include <stdio.h>
#include "array.h"

static float vectorf_sum_elements(const vectorf_type *v) {
  printf("Summing elements of vector.\n");
  float sum_value = 0;
  for (size_t idx = 0; idx < v->size; idx++)
    sum_value += v->data[idx];
  return sum_value;
}

int main() {
  size_t kVectorSize = 100;
  vectorf_type *v = vectorf_from_const(kVectorSize, 1.f);

  float sum_value = vectorf_sum_elements(v);
  printf(":: Result of summation of vector elements %f\n", sum_value);

  free_vectorf(&v);
}
