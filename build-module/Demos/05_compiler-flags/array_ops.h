/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

#ifndef DEMOS_05_COMPILER_FLAGS_ARRAY_OPS_H_
#define DEMOS_05_COMPILER_FLAGS_ARRAY_OPS_H_

#include "array.h"

vectorf_type* saxpy_vectorf(const vectorf_type* restrict v1,
                            const vectorf_type* restrict v2,
                            vectorf_type*                v_result,
                            const float                  value);

#endif // DEMOS_05_COMPILER_FLAGS_ARRAY_OPS_H_
