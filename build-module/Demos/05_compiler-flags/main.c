/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

// main.c
#include "array.h"
#include "array_ops.h"
#include <stdio.h>
#include <time.h>

int main() {
  const size_t kNumRepeats = 10;
  const size_t kVectorSize = 1 << 26;
  const float kScaleValue = 1.0f;
  vectorf_type *v1 = vectorf_from_const(kVectorSize, 1.0f);
  vectorf_type *v2 = vectorf_from_const(kVectorSize, 2.0f);
  vectorf_type *v_result = vectorf_from_const(kVectorSize, 0.0f);

  // timing analysis
  clock_t time_start, time_end;

  float total_avg_time = 0;
  for (size_t irep = 0; irep < kNumRepeats; irep++) {
    time_start = clock();
    v_result = saxpy_vectorf(v1, v2, v_result, kScaleValue);
    time_end = clock();
    total_avg_time += (float)(time_end - time_start) / CLOCKS_PER_SEC;
  }
  total_avg_time /= kNumRepeats;
  printf("\n:: Average time [%lu runs] for saxpy: %.2f milli-seconds",
         kNumRepeats, total_avg_time * 1e+3);

  free_vectorf(v1);
  free_vectorf(v2);
  free_vectorf(v_result);
}
