/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

#include <Eigen/Dense>
#include <chrono>
#include <iostream>

// Matrix size
static const uint64_t kMatrixSize = 1 << 12;

int main() {
    Eigen::MatrixXf m = Eigen::MatrixXf::Random(kMatrixSize, kMatrixSize);

    auto            time_start = std::chrono::steady_clock::now();
    Eigen::MatrixXf mxm        = m * m;
    auto            time_end   = std::chrono::steady_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(
                        time_end - time_start)
                        .count();

    std::cout
        << "CPU time needed for matrix-matrix multiplication [milliseconds]: "
        << duration << std::endl;

    return 0;
}
