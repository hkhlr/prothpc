/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

// array_ops.cc

#include "array.h"

vectorf_type* saxpy_vectorf(const vectorf_type* restrict v1,
                            const vectorf_type* restrict v2,
                            vectorf_type*                v_result,
                            const float                  value) {
    for (size_t idx = 0; idx < v_result->size; idx++)
        v_result->data[idx] = value * v1->data[idx] + v2->data[idx];
    return v_result;
}
