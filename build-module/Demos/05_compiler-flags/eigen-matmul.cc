/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

#include <Eigen/Dense>
#include <ctime>
#include <iostream>

// Matrix size
static const uint64_t kMatrixSize = 1 << 12;

int main() {
    Eigen::MatrixXf m = Eigen::MatrixXf::Random(kMatrixSize, kMatrixSize);

    std::clock_t    ctime_start = std::clock();
    Eigen::MatrixXf mxm         = m * m;
    std::clock_t    ctime_end   = std::clock();
    std::cout
        << "CPU time needed for matrix-matrix multiplication [milliseconds]: "
        << 1000.0 * static_cast<float>(ctime_end - ctime_start) / CLOCKS_PER_SEC
        << std::endl;

    return 0;
}
