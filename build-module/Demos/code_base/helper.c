/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

// helper.c
#include <stdio.h>
#include <stdlib.h>

void print_vector(const float* vec, const size_t size) {
    printf("[");
    for (size_t idx = 0; idx < size; idx++)
        printf(" %f ", vec[idx]);
    printf("]\n");
}

void print_matrix(const float* mat,
                  const size_t num_rows,
                  const size_t num_cols) {
    for (size_t idx = 0; idx < num_rows; idx++) {
        printf("[");
        for (size_t jdx = 0; jdx < num_cols; jdx++)
            printf(" %f ", mat[idx * num_cols + jdx]);
        printf("]\n");
    }
}
