/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

// vector.h
#ifndef DEMOS_01_INCLUDES_AND_DEFINES_VECTOR_H_
#define DEMOS_01_INCLUDES_AND_DEFINES_VECTOR_H_

#include <stdlib.h>

struct vector_float {
  size_t size;
  float *data;
};

struct matrix_float {
  size_t nrows, ncols;
  float *data;
};

typedef struct vector_float vectorf_type;
typedef struct matrix_float matrixf_type;

vectorf_type *malloc_vectorf(size_t size);
matrixf_type *malloc_matrixf(size_t nrows, size_t ncols);

vectorf_type *vectorf_from_array(const float *array, const size_t size);
matrixf_type *matrixf_from_array(const float *array, const size_t nrows,
                                 const size_t ncols);

vectorf_type *vectorf_random(const size_t size, const float lbound,
                             const float ubound);

vectorf_type *vectorf_from_const(const size_t size, const float value);

void free_vectorf(vectorf_type *v);
void free_matrixf(matrixf_type *m);

#endif // DEMOS_01_INCLUDES_AND_DEFINES_VECTOR_H_
