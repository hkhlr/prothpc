/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

// vector.c
#include "array.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

vectorf_type *malloc_vectorf(size_t size) {
  printf("\nAllocating a vector of size %d\n", (int)size);
  vectorf_type *v = (vectorf_type *)malloc(sizeof(vectorf_type));
  v->size = size;
  v->data = (float *)malloc(v->size * sizeof(float));
  return v;
}

matrixf_type *malloc_matrixf(size_t nrows, size_t ncols) {
  printf("\nAllocating a matrix of size (%d, %d)\n", (int)nrows, (int)ncols);
  matrixf_type *m = (matrixf_type *)malloc(sizeof(matrixf_type));
  m->nrows = nrows;
  m->ncols = ncols;
  m->data = (float *)malloc(m->nrows * m->ncols * sizeof(float));
  return m;
}

vectorf_type *vectorf_from_array(const float *array, const size_t size) {
  vectorf_type *v = malloc_vectorf(size);
  for (size_t idx = 0; idx < size; idx++)
    v->data[idx] = array[idx];
  return v;
}

matrixf_type *matrixf_from_array(const float *array, const size_t nrows,
                                 const size_t ncols) {
  matrixf_type *m = malloc_matrixf(nrows, ncols);
  for (size_t idx = 0; idx < nrows; idx++)
    for (size_t jdx = 0; jdx < ncols; jdx++)
      m->data[idx * ncols + jdx] = array[idx * ncols + jdx];
  return m;
}

vectorf_type *vectorf_random(const size_t size, const float lbound,
                             const float ubound) {
  vectorf_type *v = malloc_vectorf(size);
  // range of extend of random numbers
  float rand_range = ubound - lbound;
  // seed
  srand((unsigned int)time(NULL));
  for (size_t idx = 0; idx < size; idx++) {
    float scale = (float)rand() / (float)RAND_MAX;
    float rand_value = lbound + scale * rand_range;
    v->data[idx] = rand_value;
  }
  return v;
}

vectorf_type *vectorf_from_const(const size_t size, const float value) {
  vectorf_type *v = malloc_vectorf(size);
  for (size_t idx = 0; idx < size; idx++)
    v->data[idx] = value;
  return v;
}

void free_vectorf(vectorf_type *v) {
  printf("\nDeallocating vector.\n");
  free(v->data);
  free(v);
}

void free_matrixf(matrixf_type *m) {
  printf("\nDeallocating matrix.\n");
  free(m->data);
  free(m);
}
