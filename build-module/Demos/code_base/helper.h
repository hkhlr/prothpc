/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

// helper.h
#ifndef DEMOS_CODE_BASE_HELPER_H_
#define DEMOS_CODE_BASE_HELPER_H_

#include <stdlib.h>

void print_vector(const float* vec, const size_t size);

void print_matrix(const float* mat,
                  const size_t num_rows,
                  const size_t num_cols);

#endif // DEMOS_CODE_BASE_HELPER_H_
