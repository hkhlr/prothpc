/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR) 
   SPDX-License-Identifier: MIT */

// main.c
#include "array.h"
#include "array_ops.h"
#include <stdio.h>

int main() {
  const size_t kVectorSize = 100;
  const float kLBound = -1.f, kUBound = 1.f;
  vectorf_type *v1 = vectorf_random(kVectorSize, kUBound, kLBound);
  vectorf_type *v2 = vectorf_random(kVectorSize, kUBound, kLBound);

  vectorf_type *v_sum = add_vectorf(v1, v2);
  v_sum = scale_vectorf(v_sum, 0.5f);

  free_vectorf(v1);
  free_vectorf(v2);
  free_vectorf(v_sum);
}
