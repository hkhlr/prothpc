/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

// array_ops.cc

#include "array.h"

vectorf_type* scale_vectorf(vectorf_type* v, const float factor) {
    for (size_t idx = 0; idx < v->size; idx++)
        v->data[idx] *= factor;
    return v;
}

vectorf_type* add_vectorf(const vectorf_type* v1, const vectorf_type* v2) {
    vectorf_type* v_sum = malloc_vectorf(v1->size);
    for (size_t idx = 0; idx < v_sum->size; idx++)
        v_sum->data[idx] = v1->data[idx] + v2->data[idx];
    return v_sum;
}
