! SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
! SPDX-License-Identifier: MIT

module array_ops
    use iso_fortran_env
    implicit none

    type matrix_real64
        integer :: nrows, ncols
        real(kind=real64), allocatable :: data(:,:)
    end type

    contains

    subroutine matrix64_from_const(mat, const_value)
        use iso_fortran_env
        implicit none

        type(matrix_real64), intent(inout) :: mat
        real(kind=real64),  intent(in)    :: const_value

        write(output_unit, *) 'Initialising matrix with value ', const_value

        block
            integer :: i, j
            do j = 1, mat%ncols
                do i = 1, mat%nrows
                    mat%data(i, j) = const_value
                end do
            end do
        end block    

    end subroutine

    subroutine sum_matrix64(mat, sum_value)
        use iso_fortran_env
        implicit none
        type(matrix_real64), intent(in) :: mat
        real(kind=real64),   intent(out) :: sum_value

        sum_value = 0._real64

        block
            integer :: i, j
            do j = 1, mat%ncols
                do i = 1, mat%nrows
                    sum_value = sum_value + mat%data(i, j)
                end do
            end do
        end block

    end subroutine

end module

program test
    use iso_fortran_env
    use array_ops
    implicit none

    integer, parameter  :: nrows = 3, ncols = 3
    type(matrix_real64) :: mat
    real(kind=real64)   :: sum_value
        
    mat%nrows = nrows
    mat%ncols = ncols
    allocate( mat%data(mat%nrows, mat%ncols) )
    
    call matrix64_from_const(mat, 1._real64)
    call sum_matrix64(mat, sum_value)

    write(OUTPUT_UNIT, *) 'Result of summation of matrix values ', sum_value

    if (allocated(mat%data)) deallocate(mat%data)
end program
