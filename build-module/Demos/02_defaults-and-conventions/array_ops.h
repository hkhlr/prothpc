/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

#ifndef DEMOS_02_DEFAULTS_AND_CONVENTIONS_ARRAY_OPS_H_
#define DEMOS_02_DEFAULTS_AND_CONVENTIONS_ARRAY_OPS_H_

#include "array.h"

vectorf_type* scale_vectorf(vectorf_type* v, const float factor);
vectorf_type* add_vectorf(const vectorf_type* v1, const vectorf_type* v2);

#endif // DEMOS_02_DEFAULTS_AND_CONVENTIONS_ARRAY_OPS_H_
