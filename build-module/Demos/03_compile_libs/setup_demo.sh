# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT

#!/bin/bash

if [ $# -eq 1 ]; then
    if [ "$1" == "--prepare" ]; then
        rsync -av ../code_base/array.* ./
        rsync -av ../00_toy-project/{*.c,*.h} ./
    elif [ "$1" == "--clean" ]; then 
        find . -type f \( -name '*.c' -o -name '*.h' \) -print -delete
    fi
else
    echo "ERROR - allowed args are '--prepare' OR '--clean'."
    exit
fi
