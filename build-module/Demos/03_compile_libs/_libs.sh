# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT

#!/bin/bash

ObjPathStatic="./ObjStatic"
ObjPathDynamic="./ObjDynamic"
LibPath="./Lib"

if [ $# -eq 1 ]; then
    
    if [ "$1" == "--build" ]; then
        
        bash setup_demo.sh --prepare

        mkdir -vp build
        cd build

        CSourcesLib="array.c helper.c linalg.c"
        
        [ ! -d ${ObjPathStatic} ] && mkdir -vp ${ObjPathStatic}
        [ ! -d ${ObjPathDynamic} ] && mkdir -vp ${ObjPathDynamic}
        [ ! -d ${LibPath} ] && mkdir -vp ${LibPath}
        
        CC=$(which gcc)
        AR=$(which ar)
        
        ### Build archive file aka 'static library'
        for source_file in ${CSourcesLib}; 
        do
            ${CC} -c ../${source_file} -o ${ObjPathStatic}/${source_file//.c/.o}
        done
        
        ${AR} rcsv ${LibPath}/libarray.a ${ObjPathStatic}/*.o
        
        ### Build dynamic library
        for source_file in ${CSourcesLib};
        do
            ${CC} -fPIC -c ../${source_file} -o ${ObjPathDynamic}/${source_file//.c/.o}
        done
        
        ${CC} -shared -o ${LibPath}/libarray.so ${ObjPathDynamic}/*.o 
        
        ${CC} -c ../main.c -o main.o
        ${CC} -o program_static.exe main.o ${LibPath}/libarray.a
        ${CC} -o program_dynamic.exe main.o -L${LibPath} -larray
    
    elif [ "$1" == "--clean" ]; then 
        
        bash setup_demo.sh --clean
        [ -d build ] && rm -Rvf build

    else
        
        echo "ERROR - allowed args are '--build' OR '--clean'."
        exit
    
    fi

fi
