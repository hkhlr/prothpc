/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR) 
   SPDX-License-Identifier: MIT */

// main.c
#include "array.h"
// #include <array.h>
#include <stdio.h>

#ifndef VEC_SIZE
#define VEC_SIZE 256
#endif

int main() {
  vectorf_type *fvec = malloc_vectorf(VEC_SIZE);
  free_vectorf(fvec);
}
