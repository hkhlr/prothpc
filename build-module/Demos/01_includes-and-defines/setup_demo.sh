# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT

#!/bin/bash

if [ $# -eq 1 ]; then
    if [ "$1" == "--prepare" ]; then
        rsync -av ../code_base/array.* ./Src/
    elif [ "$1" == "--clean" ]; then 
        find . -type f -name 'array.*' -print -delete
    fi
else
    echo "ERROR - allowed args are '--prepare' OR '--clean'."
    exit
fi
