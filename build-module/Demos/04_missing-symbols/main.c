/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

// main.c
#include "array.h"
#include "array_ops.h"
#include "helper.h"
#include <math.h>
#include <stdio.h>

int main() {
  const size_t kVectorSize = 10;
  vectorf_type *v = vectorf_from_const(kVectorSize, 0.f);

  printf(":: Initial vector.\n");
  print_vector(v->data, v->size);
  printf(":: Applying 'cos' function to vector.\n");
  v = apply_to_vectorf(v, cos);
  print_vector(v->data, v->size);
  printf(":: Applying 'exp' function to vector.\n");
  v = apply_to_vectorf(v, exp);
  print_vector(v->data, v->size);

  free_vectorf(v);
}
