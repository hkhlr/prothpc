/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

// array_ops.cc

#include "array.h"

vectorf_type* apply_to_vectorf(vectorf_type* v, double (*f)(double)) {
    for (size_t idx = 0; idx < v->size; idx++) {
        float new_value = (*f)(v->data[idx]);
        v->data[idx]    = new_value;
    }
    return v;
}
