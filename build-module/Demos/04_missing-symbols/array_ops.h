/* SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
   SPDX-License-Identifier: MIT */

#ifndef DEMOS_03_MISSING_SYMBOLS_ARRAY_OPS_H_
#define DEMOS_03_MISSING_SYMBOLS_ARRAY_OPS_H_

#include "array.h"

vectorf_type* apply_to_vectorf(vectorf_type* v, double (*f)(double));

#endif // DEMOS_03_MISSING_SYMBOLS_ARRAY_OPS_H_
