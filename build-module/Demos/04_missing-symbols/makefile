# SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
# SPDX-License-Identifier: MIT

.SUFFIXES:
.SUFFIXES: .c .h

all: cprogram

sync_files:
	bash ./setup_demo.sh --prepare

CSources   = $(wildcard *.c) 
CObjects   = $(CSources:%.c=%.o)
CXXSources = $(wildcard *.cc) 
CXXObjects = $(CXXSources:%.cc=%.o)


ExeFile  = program.exe

CC     = gcc
# CFLAGS = -Wall -Wextra -Werror -pedantic -O0 -g 
CFLAGS = -Wall -Wextra -pedantic -O0 -g 
LIBS   = -lm

vpath %.c ./
vpath %.h ./

cprogram: $(ExeFile)

$(ExeFile): $(CXXObjects) $(CObjects)
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS)

%.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@
%.o : %.cc
	$(CC) -xc $(CFLAGS) -c $< -o $@


.PHONY: clean
clean:
	@rm -fv *.o $(ExeFile)
	@bash ./setup_demo.sh --clean
