// SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <assert.h>

int main () {
  const int length=16;
  int pad1=0xFA;
  int i=1;
  int j=2;
  int array[16]={100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115};
  int k=3;
  int l=4;
  int pad3=0xFC;
  int pad4=0xFD;
  
  printf("Use of variables in order %i %i %i %i %i %i %i %i %i\n", length,i,j,pad1,array[0],k,l,pad3,pad4);

  for (k=0;k<2;k++) { 
    for (j=0;j<2;j++) {
      for (l=0;l<2;l++) { 
        for (i=0;i<=length;i++) { 
          array[i]=0;
          assert (pad1==0xFA);
          assert (pad3==0xFC);
        }
      }
    }
  }

  printf("-------------------------------------\n");
  printf("Info regarding adresses of variables:\n");
  printf("Sizeof(int)=%i\n",sizeof(int));
  printf("Array @ %p\n",array);
  printf("pad4      @ %i\n",((void*)&pad4-(void*)&array)/4);
  printf("pad3      @ %i\n",((void*)&pad3-(void*)&array)/4);
  printf("l         @ %i\n",((void*)&l-(void*)&array)/4);
  printf("k         @ %i\n",((void*)&k-(void*)&array)/4);
  printf("Array     @ %i\n",(&array-&array)/4);
  printf("Array[0]  @ %i\n",((void*)(array)-(void*)&array)/4);
  printf("Array[%i] @ %i\n",length-1,((void*)(&(array[length-1]))-(void*)&array)/4);
  printf("j         @ %i\n",((void*)&j-(void*)&array)/4);
  printf("i         @ %i\n",((void*)&i-(void*)&array)/4);
  printf("pad1      @ %i\n",((void*)&pad1-(void*)&array)/4);
  printf("length    @ %i\n",((void*)&length-(void*)&array)/4);
  
  printf("Done\n");
}
