module arithmetic
    public :: add
    contains
    subroutine add( a, b, result )
        integer, intent( in )  :: a, b
        integer, intent( out ) :: result
        result = a + b
    end subroutine add
end module arithmetic
subroutine aFunction(n)
  integer, intent(in) :: n
  print *, 'Some Output: ', n
end subroutine aFunction
program demo
  call aFunction(10)
end program demo
