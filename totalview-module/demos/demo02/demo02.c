// SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#define NUMBER_OF_ITEMS 12


#define DATA_SIZE (4*1024-4-4-8-8)

struct Item
{
    float value;
    int id;
    char data[512];
    struct Item *next,* previous;
};

void addItemData(struct Item *list)
{
    int i;
    struct Item *aux = NULL;
    for (i = 0; i <= NUMBER_OF_ITEMS; i++)
    {
        list[i].next = list + i + 1;
        list[i].previous = list + i -1;
        list[i].id = i + 1;
        list[i].value = 1.f * (i+1) * 500.50;
        memset(list[i].data,0,512);
    }
}

void printItemData(struct Item *list)
{
    int i;
    for (i = 0; i <= NUMBER_OF_ITEMS; i++)
    {
        printf("=========Item %d=========\n", list[i].id);
        printf("Value: %.2f \n", list[i].value);
        printf("Previous Item %d\n", list[i].previous->id);
        printf("    Next Item %d\n", list[i].next->id);
    }
}

int main()
{

    struct Item * list;
    list = malloc ( sizeof (struct Item) * NUMBER_OF_ITEMS);
    
    if (list = NULL) exit(-1);    
    
    addItemData(list);

    printItemData(list);

    return 0;
}
