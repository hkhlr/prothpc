// SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
// SPDX-License-Identifier: MIT

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

#define NUM_MESSAGES 10
#define TAG 42

int main (int argc, char *argv[])
{
	int a;
	float b;
	
int rank,size;
MPI_Init(&argc,&argv);
MPI_Comm_size(MPI_COMM_WORLD, &size);
MPI_Comm_rank(MPI_COMM_WORLD, &rank);

// rank 0 sends messages
if (rank == 0) {
  for (int i=0; i<NUM_MESSAGES; i++) {
    a = i*10;// message content
	MPI_Send(&a,1,MPI_INT,1,TAG,MPI_COMM_WORLD);
    
    printf("Task %i sent %i\n",rank,a);
    }
  }


// rank 1 reveive messages
if (rank == 1) {
  for (int i=0; i<NUM_MESSAGES; i++) {
	MPI_Recv(&b,1,MPI_FLOAT,0,TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
	printf("Rank %i received %f\n",rank,b);
    }
  }

MPI_Finalize();
}

