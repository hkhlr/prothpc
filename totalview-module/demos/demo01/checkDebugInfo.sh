#!/bin/sh

for file in $@
do
  if [ -x $file ]; then
    output=`readelf --debug-dump=info $file | grep "DW_TAG_compile_unit" -A7 | grep -e "DW_AT_name" -e "DW_AT_comp_dir" | sed -e"s/^.*://g" | tr -d " "| awk  'NR % 2 == 1 {o=$0;next}{print $0 "/" o}'
# | grep "DW_TAG_compile_unit" -A7 | grep -e "DW_AT_name" -e "DW_AT_comp_dir" | sed -e"s/^.*://g" | tr -d " "| awk  'NR % 2 == 1 {o=$0;next}{print o " : " $0}' | awk '{FS=":"};{printf("%s/%s\n", $8,$4)}'`
    if [[ -z "$output" ]]; then
      echo "No Debuginformation found in $file"
    else
      echo -e "\"$file\" contains debug information.\nThe following source files were compiled with -g:"
      for i in $output; do 
        echo -e "\t$output"
      done
    fi
  else
    echo File $file not found
  fi
  echo -n -e "\n"
done
