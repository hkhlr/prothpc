// SPDX-FileCopyrightText: 2021 Competence Center for High Performance Computing in Hessen (HKHLR)
// SPDX-License-Identifier: MIT

#include <vector>
#include <iostream>
#include <iomanip>

class Matrix:public std::vector<std::vector <double> > {
  public:
    Matrix(int irows,int icolumns){
      resize(irows);
      for (int i=0;i<irows;i++) 
        operator[](i).resize(icolumns);
    }  
};

std::ostream &operator<<(std::ostream &os,Matrix const &m){
  std::cout <<"["<<m.size() << "x" << m[0].size() << "]:\n";
  for (auto i: m) {
    for (auto j: i) {
      std::cout << std::setw(3)<< j << " ";
    }
    std::cout << "\n";  
  }

  return os;
}
long int verboseFactorialImpl(long int i) {
  long int tmpResult=0;
  if (i==1) {
    tmpResult=1;
    std::cout << "verboseFactorialImpl("<<i<<") = 1"<<std::endl;
  } else {
    std::cout << "verboseFactorialImpl("<<i<<") = verboseFactorialImpl("<<i-1<<") * "<<i<<std::endl;
    tmpResult=verboseFactorialImpl(i-1)*i;
  }
  return tmpResult;  
}

long int verboseFactorial(long int i) {
  // check for errors
  if (i<0) {
    std::cerr << "Factorial of " << i << " not defined\n" << std::endl;
    throw std::string("Factorial of i < 0 not implemented");
    return 0;
  }
  return verboseFactorialImpl(i);
}



int main (int argc, char *argv[])
  {
  bool continueLoop=false;
  int iterations;
  std::vector<std::string> arguments(argv + 1, argv + argc);
  std::cout << "\nStarting Demo" << std::endl;
  
  if (getenv("ProtHPC_ENV")!=NULL)
    std::cout << "The environment variable is: " << getenv("ProtHPC_ENV") << std::endl;
  else
    std::cout <<"The ProtHPC_ENV variable is not set" << std::endl;

  if (arguments.size()>0) {
    std::cout << "There are " << arguments.size() << " arguments passed to the program.\n";
    std::cout << "Commandline is :" << argv[0];
    for (auto arg: arguments) std::cout << " " << arg;
    std::cout << std::endl;  
    for (auto arg: arguments) {
      std::cout << "----------------------------\n";
      auto value = std::stoi(arg);
      std::cout << "verboseFactorial("<<value<<") = " <<verboseFactorial(value)<<std::endl;
    }
    std::cout << "----------------------------\n";
  }  
  else {
    std::cout <<"No arguments passed to example, reverting to STDIN." << std::endl;
    std::cout << "----------------------------\n";
    std::cout << "Reading number of iterations from input: \t";
    while (std::cin >>  iterations){
      std::cout << "\nfac("<<iterations<<")="<<verboseFactorial(iterations)<<std::endl;
      std::cout << "----------------------------\n";
      std::cout << "Reading number of iterations from input: \t";
    };
    std::cout << "EOF detected; Terminating.\n";    
  }

  std::cout << "Demo done" << std::endl;
  
  
  return 0;
}
