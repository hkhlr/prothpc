#include <iostream>
#include <cstdlib>
#include <cmath>
#include <omp.h>
#include <utility>

using namespace std;

std::pair<double,long> operator+(const std::pair<double,long> &a,const std::pair<double,long> &b){
  return std::pair<double,long>(a.first+b.first,a.second+b.second);
}

double tolerance = 1e-8;

double f(double const x) {
  return 4.0/(1.0+x*x);
}

std::pair<double,long> computeIntegral(double const x0, double const dx, double (*fptr)(double), double const fx0, int const level = 0) {
  double const x05 = x0 + 0.5*dx;
  double const fx05 = fptr(x05);
  std::pair<double,long> res(0,0);
  if (abs(fx05 - fx0) / fx0 > tolerance) {
    std::pair<double,long> s1,s2;
#pragma omp task default(shared) if((level & 3) == 0) untied
    {
      s1 = computeIntegral(x0, 0.5*dx, fptr, fx0, level + 1);
    }
#pragma omp task default(shared) if((level & 3) == 0) untied
    {
      s2 = computeIntegral(x05, 0.5*dx, fptr, fx05, level + 1);
    }
#pragma omp taskwait
    res = s1 + s2;
  } else {
    res.first=fx05 * dx;
  }
  res.second++;
  return res;
}

int main(int argc, char *argv[]) {
  if (argc > 1) {
    tolerance = atof(argv[1]);
  }
  cout.precision(16);
  std::pair<double,long> pi;
  double const t0 = omp_get_wtime();
#pragma omp parallel
  {
#pragma omp master
    {
      pi = computeIntegral(0, 1, &f, f(0));
    }
#pragma omp critical
    cout << "Thread " << omp_get_thread_num() << std::endl;
  }
  double const t1 = omp_get_wtime();
  cout << "Tolerance: " << tolerance << "\n";
  cout << "Result: " << pi.first << "\n";
  cout << "Segments: " << pi.second << "\n";
  cout << "Pi: " << M_PI << "\n";
  cout << "Rel. Error: " << abs(M_PI - pi.first) / M_PI << "\n";
  cout << "Time: " << t1 - t0 << " s\n";
}
