#include <iostream>
#include <cstdlib>
#include <cmath>
#include <omp.h>

using namespace std;

#include <utility>

using namespace std;

std::pair<double,long> operator+(const std::pair<double,long> &a,const std::pair<double,long> &b){
  return std::pair<double,long>(a.first+b.first,a.second+b.second);
}

double tolerance = 1e-8;

double f(double const x) {
  return 4.0/(1.0+x*x);
}

std::pair<double,long>  computeIntegral(double const x0, double const dx, double (*fptr)(double), double const current_sum) {
  double const ldx = dx*0.5;
  double const refined_sum=0.5*(fptr(x0)+2*fptr(x0+ldx)+fptr(x0+2*ldx));
  std::pair<double,long> res(0,0);
  if (abs(refined_sum - current_sum) / current_sum > tolerance) {


    
    auto s1 = computeIntegral(x0, ldx, fptr, refined_sum);


    
    auto s2 = computeIntegral(x0+ldx, ldx, fptr, refined_sum);
    
    res = s1+s2;
  } else {
    res.first=refined_sum*dx*0.5;
  }
  res.second++;
  return res;
}

int main(int argc, char *argv[]) {
  if (argc > 1) {
    tolerance = atof(argv[1]);
  }
  cout.precision(16);
  std::pair<double,long> pi;
  double const t0 = omp_get_wtime();




  pi = computeIntegral(0, 1, &f, f(0));




  double const t1 = omp_get_wtime();
  cout << "Tolerance: " << tolerance << "\n";
  cout << "Result: " << pi.first << "\n";
  cout << "Segments: " << pi.second << "\n";
  cout << "Pi: " << M_PI << "\n";
  cout << "Rel. Error: " << abs(M_PI - pi.first) / M_PI << "\n";
  cout << "Time: " << t1 - t0 << " s\n";
}
