#include <iostream>
#include <cstdlib>
#include <cmath>
#include <omp.h>

using namespace std;

double tolerance = 1e-8;

double f(double const x) {
  return 4.0/(1.0+x*x);
}

double integrate(double start,double end, long int samplePoints, double (*)(double x)) {
  double result = 0.0;
  double dx = (end-start) / samplePoints;
  if (samplePoints<1) return 0.0;
  for (long int i = 0; i < samplePoints; i++ ) {
    double x = i*dx - 0.5*dx;
    double tmp = f(x);
    tmp*=dx;
    result += tmp;
  }
  return result;
}










int main(int argc, char *argv[]) {
  int n = 10000;
  if (argc > 1) {
    n = ceil(atof(argv[1]));
  }
  double const t0 = omp_get_wtime();
  double result = integrate(0.0, 1.0,n,f);
  double const t1 = omp_get_wtime();
  cout.precision(16);
  cout << "Intervals: " << n << "\n";
  cout << "Result: " << result << "\n";
  cout << "Pi: " << M_PI << "\n";
  cout << "Rel. Error: " << abs(M_PI - result) / M_PI << "\n";
  cout << "Time: " << t1 - t0 << " s\n";
}
