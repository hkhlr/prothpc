#include <iostream>
#include <cstdlib>
#include <cmath>
#include <omp.h>
#include <utility>
#include <unistd.h>

using namespace std;

std::pair<double,long> operator+(const std::pair<double,long> &a,const std::pair<double,long> &b){
  return std::pair<double,long>(a.first+b.first,a.second+b.second);
}

double tolerance = 1e-8;

double f(double const x) {
  return 4.0/(1.0+x*x);
}

std::pair<double,long>  computeIntegral(double const x0, double const dx, double (*fptr)(double), long int segments) {
  double local_sum=0.0;
  if (segments<1) return std::pair<double,long> (0,0); 
  double local_dx=dx/segments;
  double local_x0;
  // local sum = dx*(f(x0+i*dx)+f(x0+(i+1)*dx))/2
  // loop invariants dx and 0.5 have been moved to return statement
#pragma omp parallel default(shared) private(local_x0) 
  { 
    #pragma omp for reduction(+:local_sum)
    for (int i=0;i<segments;i++) {
      local_x0=x0+i*local_dx;
      local_sum+=fptr(x0+i*local_dx)+fptr(x0+(i+1)*local_dx);
    }
  }
  return std::pair<double,long> (local_sum*local_dx*.5,segments);
}

int main(int argc, char *argv[]) {
  if (argc > 1) {
    tolerance = atof(argv[1]);
  }
  cout.precision(16);
  std::pair<double,long> pi;
  double const t0 = omp_get_wtime();

  
  #pragma omp parallel default(shared)  
  {
    unsigned int thread_count = omp_get_num_threads();
    #pragma omp barrier
    unsigned int thread_num = omp_get_thread_num();
    if (thread_num == 0 ) {
      cout << "This program uses " << thread_count << " threads.\n" << std::flush;
    } else {
      for (long int i=0;i< 1000000;i++) sqrt(i);
    }
    #pragma omp barrier
    cout << "This is thread #" << thread_num << ".\n";
    #pragma omp master 
    cout << std::flush;
  }


  pi = computeIntegral(0, 1, &f, 1000000);




  double const t1 = omp_get_wtime();
  cout << "Tolerance: " << tolerance << "\n";
  cout << "Result: " << pi.first << "\n";
  cout << "Segments: " << pi.second << "\n";
  cout << "Pi: " << M_PI << "\n";
  cout << "Rel. Error: " << abs(M_PI - pi.first) / M_PI << "\n";
  cout << "Time: " << t1 - t0 << " s\n";
}
